function init()
 {
  var soluteVolumeInput = document.getElementById("solute-volume");
  var soluteUnitSelect = document.getElementById("solute-unit");
  var dilutionFactorInput = document.getElementById("dilution-factor");
  var resultUnitSelect = document.getElementById("result-unit");

  soluteVolumeInput.addEventListener("input", calculate);
  soluteUnitSelect.addEventListener("change", calculate);
  dilutionFactorInput.addEventListener("input", calculate);
  resultUnitSelect.addEventListener("change", calculate);
 }

function calculate()
 {
  var soluteVolume = parseFloat(document.getElementById("solute-volume").value);
  var soluteUnit = document.getElementById("solute-unit").value;
  var dilutionFactor = parseFloat(document.getElementById("dilution-factor").value);
  var resultUnit = document.getElementById("result-unit").value;

  // Check for zero or NaN inputs
  if (isNaN(soluteVolume) || soluteVolume == 0 || isNaN(dilutionFactor) || dilutionFactor == 0)
   {
    var resultDiv = document.getElementById("result");
    resultDiv.innerHTML = "Please enter valid values for solute volume and dilution factor.";
    return;
   }

  // convert solute volume to microliters
  switch (soluteUnit)
   {
    case "microliters":
    var soluteVolume_ul = soluteVolume;
    break;
    case "milliliters":
    var soluteVolume_ul = soluteVolume * 1000;
    break;
    case "deciliters":
    var soluteVolume_ul = soluteVolume * 100000;
    break;
    case "liters":
    var soluteVolume_ul = soluteVolume * 1000000;
    break;
   }

  // calculate solvent volume in microliters
  var solventVolume_ul = (dilutionFactor - 1) * soluteVolume_ul;

  // calculate total volume in microliters
  var totalVolume_ul = soluteVolume_ul + solventVolume_ul;

  // convert result volume to the selected unit
  switch (resultUnit)
   {
    case "microliters":
    var resultVolume = totalVolume_ul;
    solventVolume_result=solventVolume_ul;
    break;
    case "milliliters":
    var resultVolume = totalVolume_ul / 1000;
    solventVolume_result=solventVolume_ul/1000;
    break;
    case "deciliters":
    var resultVolume = totalVolume_ul / 100000;
    solventVolume_result=solventVolume_ul/100000;
    break;
    case "liters":
    var resultVolume = totalVolume_ul / 1000000;
    solventVolume_result=solventVolume_ul/1000000;
    break;
   }

  var resultDiv = document.getElementById("result");
  resultDiv.innerHTML = "To dilute " + soluteVolume + " " + soluteUnit + " by a factor of " + dilutionFactor + 
  ", add " + solventVolume_result.toFixed(3) + " " + resultUnit + " of solvent and bring the total volume to "
  + resultVolume.toFixed(3) + " " + resultUnit + ".";
 }
